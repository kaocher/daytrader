# Daytrader

The project meant to download and process stock market quotes, analyse it and generate automated investment decisions. The stack used: AWS (Lambda, Glue and S3), Python, Terraform, GitLab.

